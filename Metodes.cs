﻿/*
 * AUTHOR: Armando Martínez
 * DATE: 2023 - 01 - 11
 * DESCRIPTION: Conjunt d'exercicis de parametrització
 */

using System;

namespace M03UF2ParametritzacioMartinezArmando
{
    class Metodes
    {
        static void Main(string[] args)
        {
            Start();
        }
        // Inici del programa
        static void Start()
        {
            int selectedOption;
            do
            {
                Console.WriteLine("EXERCICIS DE PARAMETRITZACIÓ");
                selectedOption = Menu();
                if (selectedOption != 0) { Console.Clear(); }
                switch (selectedOption)
                {
                    case 0:
                        Console.WriteLine("\n[EL PROGRAMA ES TANCARÀ . . .]");
                        break;
                    case 1:
                        RightTriangleSize();
                        break;
                    case 2:
                        Lamp();
                        break;
                    case 3:
                        CampsiteOrganizer();
                        break;
                    case 4:
                        BasicRobot();
                        break;
                    case 5:
                        ThreeInARow();
                        break;
                    case 6:
                        SquashEncounter();
                        break;
                }
                Console.WriteLine("\n[ENTER PER CONTINUAR]");
                Console.ReadLine();
                Console.Clear();
            } while (selectedOption != 0);
        }
        // Mostra un menú que permet escollir entre varies opcions i retorna la decisió de l'usuari
        static int Menu()
        {
            string[] options = { " 0 - Exit", " 1 - RightTriangleSize", " 2 - Lamp", " 3 - CampsiteOrganizer",
                            " 4 - BasicRobot", " 5 - ThreeInARow", " 6 - SquashCounter"};
            foreach(string option in options) { Console.WriteLine(option);}
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, 6);
            return selectedOption;
        }
        // Recull una dada que obligatoriament ha de ser un integer dins d'un cert rang
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min) { Console.WriteLine($"El número ha d'estar entre {min} i {max}!"); }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        // Recull una dada que obligatoriament ha de ser un integer positiu
        static int ParseIntPositive(string prompt)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum < 0) { Console.WriteLine($"El número ha de ser positiu!"); }
            } while (!isParsed || parsedNum < 0);
            return parsedNum;
        }
        // Recull una dada que obligatoriament ha de ser un double positiu
        static double ParseDoublePositive(string prompt)
        {
            bool isParsed;
            double parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = double.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum < 0) { Console.WriteLine($"El número ha de ser positiu!"); }
            } while (!isParsed || parsedNum < 0);
            return parsedNum;
        }
        // Recull una dada que obligatoriament ha de ser un string dins d'una llista d'opcions
        static string ParseStringInOptions(string prompt, string[] options)
        {
            string parsedString = "";
            bool isParsed = false;
            do
            {
                Console.Write(prompt);
                string userString = Console.ReadLine();
                foreach (string val in options)
                {
                    if (userString == val) { isParsed = true; parsedString = userString; }
                }
                if (!isParsed) { Console.WriteLine("\n[ERROR]\n"); }
            } while (!isParsed);
            return parsedString;
        }
        // Recull dades per l'exercici "CampsiteOrganizer"
        static string ParseStringCampsite(string prompt)
        {
            string parsedString = "";
            bool isParsed = false;
            do
            {
                Console.Write(prompt);
                string userString = Console.ReadLine();
                string[] fullString = userString.Split(' ');
                if (fullString[0] == "END") 
                {
                    parsedString = "END";
                    isParsed = true;
                }
                else if (fullString.Length == 3)
                {
                    if (fullString[0] == "ENTRA" && int.TryParse(fullString[1], out int result) && fullString[2].Length > 1)
                    {
                        parsedString = userString;
                        isParsed = true;
                    }
                }
                else if (fullString.Length == 2)
                {
                    if (fullString[0] == "MARXA" && fullString[1].Length > 1)
                    {
                        parsedString = userString;
                        isParsed = true;
                    }
                }
                if (!isParsed) { Console.WriteLine("\n[ERROR]\n"); }
            } while (!isParsed);
            return parsedString;
        }
        // Converteix un string amb espais en un array de strings
        static string[] ToStringArray(string toArray)
        {
            string[] stringArray = toArray.Split(' ');
            return stringArray;
        }
        // Converteix un string amb espais en un array de integers
        static int[] ToIntArray(string toArray)
        {
            string[] stringArray = toArray.Split(' ');
            int[] intArray = new int[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++)
            {
                int.TryParse(stringArray[i], out intArray[i]);
            }
            return intArray;
        }
        // Troba index d'un element en un array de strings
        static int IndexOf(string[] vector, string value)
        {
            int index = 0;
            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i] == value) { index = i; }
            }
            return index;
        }
        // Calcula la hipotenusa
        static double Hypotenuse(double a, double b)
        {
            double c = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
            return c;
        }
        /* --- COMANDES BASICROBOT --- */
        static void Up(ref double y, double vel) { y += vel; }
        static void Down(ref double y, double vel) { y -= vel; }
        static void Right(ref double x, double vel) { x += vel; }
        static void Left(ref double x, double vel) { x -= vel; }
        static void Accelerate(ref double vel) { vel += 0.5; }
        static void Decelerate(ref double vel) { vel -= 0.5; }
        /* --- --- --- --- --- --- --- */
        // Emplena una matrix de caràcters
        static void FillCharMatrix(char[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = '-';
                }
            }
        }
        // Visualitza una matrix de caràcters
        static void VisualizeCharMatrix(char[,] matrix)
        {
            int count = matrix.GetLength(0);
            for (int i = 0; i < matrix.GetLength(0); i++) { Console.Write($"\t{i + 1}"); }
            Console.WriteLine("\n");
            int j = 0;
            foreach(char val in matrix)
            {
                if (count == matrix.GetLength(0)) { Console.Write($" {j + 1}"); j++; }
                count--;
                if (count != 0) { Console.Write($"\t{val}"); }
                else
                {
                    Console.Write($"\t{val}\n\n");
                    count = matrix.GetLength(0);
                }

            }
        }
        // Comproba les dades introduides en el 3 en raya
        static int[] ParseIntThreeInARow(string prompt, int min, int max, char[,] board)
        {
            bool isXParsed = false;
            bool isYParsed = false;
            int parsedX = -1;
            int parsedY = -1;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                if (userNum.Length == 3)
                {
                    isXParsed = int.TryParse(Convert.ToString(userNum[0]), out parsedX);
                    isYParsed = int.TryParse(Convert.ToString(userNum[2]), out parsedY);
                    if (!isXParsed || !isYParsed || userNum[1] != ' ') { Console.WriteLine("\nEl format es incorrecte!\n"); }
                    else if (parsedX > max || parsedX < min || parsedY > max || parsedY < min) { Console.WriteLine($"\nEls números han d'estar entre {min} i {max}!\n"); }
                    else if (board[parsedY - 1, parsedX - 1] != '-') { Console.WriteLine($"La casella {parsedX}, {parsedY} ya es troba ocupada!"); }
                } else { Console.WriteLine("\nEl format es incorrecte!\n"); }

            } while (!isXParsed || !isYParsed || parsedX > max || parsedX < min || parsedY > max || parsedY < min || board[parsedY - 1, parsedX  - 1] != '-');
            return new int[] { parsedY, parsedX};
        }
        // Comproba si acaba la partida i el guanyador
        static bool CheckGameState(char[,] board)
        {
            bool gameOver = true;
            foreach (char val in board)
            {
                if (val == '-') { gameOver = false; }
            }
            if (gameOver)
            {
                Console.WriteLine("Empat! Ningú guanya!");
                return true;
            }
            else
            {
                int winner = CheckDiagonals(board);
                if (winner != 0)
                {
                    Console.WriteLine($"\nEl jugador {winner} guanya!\n");
                    VisualizeCharMatrix(board);
                    return true;
                }
                for (int i = 0; i < 3; i++)
                {
                    winner = CheckVertical(board, i);
                    if (winner != 0)
                    {
                        Console.WriteLine($"\nEl jugador {winner} guanya!\n");
                        VisualizeCharMatrix(board);
                        return true;
                    }
                    winner = CheckHorizontal(board, i);
                    if (winner != 0)
                    {
                        Console.WriteLine($"\nEl jugador {winner} guanya!\n");
                        VisualizeCharMatrix(board);
                        return true;
                    }
                }
            }
            return false;
        }
        // Comproba si una linea vertical es complerta
        static int CheckVertical(char[,] board, int vertical)
        {
            if (board[0, vertical] == board[1, vertical] && board[1, vertical] == board[2, vertical] && board[0, vertical] != '-')
            {
                if (board[0, vertical] == 'X') { return 1; }
                else { return 2; }
            }
            return 0;
        }
        // Comproba si una linea horitzontal es complerta
        static int CheckHorizontal(char[,] board, int horizontal)
        {
            if (board[horizontal, 0] == board[horizontal, 1] && board[horizontal, 1] == board[horizontal, 2] && board[horizontal, 0] != '-')
            {
                if (board[horizontal, 0] == 'X') { return 1; }
                else { return 2; }
            }
            return 0;
        }
        // Comproba si les lineas diagonals son complertas
        static int CheckDiagonals(char[,] board)
        {
            if (((board[0, 0] == board[1, 1] && board[1, 1] == board[2, 2]) || (board[0, 2] == board[1, 1] && board[1, 1] == board[2, 0])) && board[1, 1] != '-')
            {
                if (board[1, 1] == 'X') { return 1; }
                else { return 2; }
            }
            return 0;
        }
        // Comproba la quantitat de punts de Squash en base a un string
        static int[] CheckPoints(string game)
        {
            int a = 0;
            int b = 0;
            // FALTEN COSES CONDICIONS
            foreach(char val in game)
            {
                if (val == 'A') { a++; }
                else if (val == 'B') { b++; }
            }
            return new int[2] { a, b };
        }
        //
        static string GameParse()
        {
            string parsedString = "";
            bool isParsed;
            do
            {
                isParsed = true;
                parsedString = Console.ReadLine();
                foreach(char val in parsedString)
                {
                    if (val != 'A' && val != 'B' && val != 'F') { isParsed = false; }
                }
            } while (!isParsed);
            return parsedString;
        }
        /*************************************
         * * *     E X E R C I C I S     * * *
         *************************************/

        // Ens diu l'àrea i el perímetre d'una llista de traingles rectangles
        static void RightTriangleSize()
        {
            int triangleNum = ParseIntPositive("Introdueix un número de triangles: ");
            double[] heights = new double[triangleNum];
            double[] widths = new double[triangleNum];
            for (int i = 0; i < triangleNum; i++)
            {
                heights[i] = ParseDoublePositive($"Introdueix l'alçada del triangle {i + 1}: ");
                widths[i] = ParseDoublePositive($"Introdueix la base del triangle {i + 1}: ");
            }
            for (int i = 0; i < triangleNum; i++)
            {
                double area = Math.Round(heights[i] * widths[i] / 2, 2);
                double perimeter = Math.Round(heights[i] + widths[i] + Hypotenuse(heights[i], widths[i]), 2);
                Console.WriteLine($"\nTRIANGLE {i + 1}:\n\tAREA: {area}\n\tPERIMETER: {perimeter}");
            }
        }
        // Simula una làmpada
        static void Lamp()
        {
            string action;
            bool isOn = true;
            do
            {
                action = ParseStringInOptions("> ", new string[] { "TURN ON", "TURN OFF", "TOGGLE", "END" });
                switch (action)
                {
                    case "TURN ON":
                        isOn = true;
                        Console.WriteLine($"\n{isOn}\n");
                        break;
                    case "TURN OFF":
                        isOn = false;
                        Console.WriteLine($"\n{isOn}\n");
                        break;
                    case "TOGGLE":
                        isOn = !isOn;
                        Console.WriteLine($"\n{isOn}\n");
                        break;
                    case "END":
                        break;
                }
            } while (action != "END");
        }
        // Manté un compte de parcel·les ocupades i persones dins d'un càmping
        static void CampsiteOrganizer()
        {
            string commandString;
            string reservationNames = "none";
            string peopleNums = "0";
            do
            {
                int parceles = 0;
                int persones = 0;
                commandString = ParseStringCampsite("> ");
                string[] commands = commandString.Split(' ');
                if (commands.Length == 3)
                {
                    reservationNames += " " + commands[2];
                    peopleNums += " " + commands[1];
                }
                else if (commands.Length == 2)
                {
                    int index = IndexOf(ToStringArray(reservationNames), commands[1]);
                    string[] reservationNamesArray = ToStringArray(reservationNames);
                    int[] peopleNumsArray = ToIntArray(peopleNums);
                    reservationNamesArray[index] = "none";
                    peopleNumsArray[index] = 0;
                    reservationNames = "none";
                    peopleNums = "0";
                    for (int i = 1; i < reservationNamesArray.Length; i++)
                    {
                        reservationNames += " " + reservationNamesArray[i];
                        peopleNums += " " + Convert.ToString(peopleNumsArray[i]);
                    }
                }
                if (commandString != "END")
                {
                    foreach(string val in ToStringArray(reservationNames))
                    {
                        if (val != "none") { parceles++; } 
                    }
                    foreach(int val in ToIntArray(peopleNums)) { persones += val; }
                    Console.WriteLine($"\nparcel·les: {parceles}\n\npersones: {persones}\n");
                }
            } while (commandString != "END");
        }
        // Simula un robot que es mou en un espai 2D
        static void BasicRobot()
        {
            double x = 0.0;
            double y = 0.0;
            double velocity = 1.0;
            string command;
            do
            {
                command = ParseStringInOptions("> ", new string[]
                { "DALT", "BAIX", "DRETA", "ESQUERRA", "ACCELERAR", "DISMINUIR", "POSICIO", "VELOCITAT", "END"});
                switch (command)
                {
                    case "DALT":
                        Up(ref y, velocity);
                        break;
                    case "BAIX":
                        Down(ref y, velocity);
                        break;
                    case "DRETA":
                        Right(ref x, velocity);
                        break;
                    case "ESQUERRA":
                        Left(ref x, velocity);
                        break;
                    case "ACCELERAR":
                        if (velocity < (10 - 0.5)) { Accelerate(ref velocity); }
                        else { Console.WriteLine("\n[ERROR]\n"); }
                        break;
                    case "DISMINUIR":
                        if (velocity > (0 + 0.5)) { Decelerate(ref velocity); }
                        else { Console.WriteLine("\n[ERROR]\n"); }
                        break;
                    case "POSICIO":
                        Console.WriteLine($"\nLa posició del robot és ({x}, {y})");
                        break;
                    case "VELOCITAT":
                        Console.WriteLine($"\nLa velocitat del robot és {velocity}");
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
            } while (command != "END");
        }
        // 3 en raya
        static void ThreeInARow()
        {
            int player = 1;
            char[,] board = new char[3, 3];
            FillCharMatrix(board);
            Console.WriteLine($"Juga al 3 en linia!");
            bool finished = false;
            do
            {
                if (player == 1) { Console.ForegroundColor = ConsoleColor.Blue; }
                else { Console.ForegroundColor = ConsoleColor.Red; }
                Console.WriteLine($"Jugador {player}, introdueix una fitxa [x y]:\n");
                Console.ForegroundColor = ConsoleColor.White;
                VisualizeCharMatrix(board);
                int[] coordinates = ParseIntThreeInARow("> ", 1, 3, board);
                if (player == 1)
                {
                    board[coordinates[0] - 1, coordinates[1] - 1] = 'X';
                    player = 2;
                }
                else
                {
                    board[coordinates[0] - 1, coordinates[1] - 1] = 'O';
                    player = 1;
                }
                finished = CheckGameState(board);
                if (finished) { Console.WriteLine("Bona partida!"); }
                else 
                {
                    Console.Clear();
                }
            } while (!finished);
        }
        // Squash o algo, no sé la verdad
        static void SquashEncounter()
        {
            int setInt = ParseIntPositive("");
            int[,] setPoints = new int[setInt, 2];
            for (int i = 0; i < setInt; i++)
            {
                
                string game = GameParse();
                int[] points = CheckPoints(game);
                setPoints[i, 0] = points[0];
                setPoints[i, 1] = points[1];
            }
            for (int i = 0; i < setInt; i++)
            {
                Console.WriteLine($"{setPoints[i, 0]}-{setPoints[i, 1]}");
            }
        }
    }
}